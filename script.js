	$(document).ready(function() {
		$('.js-slick').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000,
			dots: true,
			arrows: false,

			infinite: true,
		});

		$(".js-slick-skin , .js-slick-note , .js-slick-wallpaper").slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			dots: false,
			arrows: true,
			infinite: true,
			prevArrow: "<img class='slick-prev' src='assets/prev-slide.png'>",
			nextArrow: "<img class='slick-next' src='assets/next-slide.png'>",
		});
		$(".js-slick-video").slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: false,
			
			dots: false,
			arrows: true,
			infinite: true,
			prevArrow: "<img class='slick-prev' src='assets/prev-slide.png'>",
			nextArrow: "<img class='slick-next' src='assets/next-slide.png'>",
		});
		$(".menu-items li").click(function () {
			var check_data = $(this).attr("data-id");
			$(".tab_check").addClass("hide");
			$(".menu-items li").removeClass("active");
			$(this).addClass("active");
			$(".tab__skin" + check_data).removeClass("hide").removeClass("active");
		});

		$(".skin__detail>div").click(function () {
			var check_url = $(this).attr("data-url");

			var add_video = "<iframe  src='https://www.youtube.com/embed/" + check_url + "' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
			$(this).parent().parent().find(".skin__video").html(add_video);
		});
	});